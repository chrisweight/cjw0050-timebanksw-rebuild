import { Route } from '@angular/router';
import { OnboardedGuard } from './guards/onboarded.guard';
import { EventResolver } from './resolvers/event.resolver';
import { FaqResolver } from './resolvers/faq.resolver';
import { PostResolver } from './resolvers/post.resolver';

export const routes: Route[] = [
	{
		path: '',
		loadChildren: () =>
			import('./tabs').then(m => m.routes),
		canActivate: [OnboardedGuard]
	},
	{
		path: 'onboarding',
		loadComponent: () =>
			import('./onboarding').then(m => m.OnboardingPageComponent)
	},
	{
		path: 'faq/:id',
		loadComponent: () => import('./faq').then(m => m.FaqPageComponent),
		resolve: {
			faq: FaqResolver
		}
	},
	{
		path: 'event/:id',
		loadComponent: () =>
			import('./event').then(m => m.EventPageComponent),
		resolve: {
			event: EventResolver,
		}
	},
	{
		path: 'story/:id',
		loadComponent: () =>
			import('./post').then(m => m.PostPageComponent),
		resolve: {
			post: PostResolver
		}
	},
	{
		path: 'news/:id',
		loadComponent: () =>
			import('./post').then(m => m.PostPageComponent),
		resolve: {
			post: PostResolver,
		}
	}
];
