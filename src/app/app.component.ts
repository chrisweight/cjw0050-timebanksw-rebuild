import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { App, AppState, BackButtonListenerEvent, RestoredListenerEvent, URLOpenListenerEvent } from '@capacitor/app';
import { SplashScreen } from '@capacitor/splash-screen';
import { IPushData } from '@services';
import { Broadcaster } from '@utils';
import { IonApp, IonRouterOutlet } from '@ionic/angular/standalone';

@Component({
    selector: 'app-root',
    standalone: true,
    imports: [IonApp, IonRouterOutlet],
    template: `
    <ion-app>
      <ion-router-outlet></ion-router-outlet>
    </ion-app>
  `
})
export class AppComponent {
  constructor(private broadcast: Broadcaster, private router: Router) {
    this.init();
  }

  async init() {
    await SplashScreen.hide();

    this.broadcast
      .subscribe(
        'PushNotificationOpened',
        (payload: IPushData) => this.router.navigate([`${payload.postType}/${payload.postId}`])
      );

    App.addListener('backButton', (data: BackButtonListenerEvent) => {
      console.log('App Back Button press detected', data);
    });

    App.addListener('appStateChange', (state: AppState) => {
      console.log('App state changed. Is active?', state.isActive);
      console.log(JSON.stringify(state));
    });

    App.addListener('appUrlOpen', (data: URLOpenListenerEvent) => {
      console.log('App opened with URL: ' + data.url);
    });

    App.addListener('appRestoredResult', (data: RestoredListenerEvent) => {
      console.log('Restored state:', data);
    });
  };
}
