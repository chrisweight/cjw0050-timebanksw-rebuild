import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SettingsPageComponent } from './settings.page';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { ModalController } from '@ionic/angular/standalone';

describe('SettingsPage', () => {
  let component: SettingsPageComponent;
  let fixture: ComponentFixture<SettingsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SettingsPageComponent],
      providers: [
        Diagnostic, 
      ]
    }).compileComponents();

    TestBed.overrideComponent(SettingsPageComponent, {
      set: {
          providers: [
            {
              provide: ModalController, 
              useValue: () => { 
                dismiss: () => Promise.resolve() 
              }
            }
        ],
      },
    });
    
    fixture = TestBed.createComponent(SettingsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
