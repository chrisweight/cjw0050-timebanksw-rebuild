import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { App } from '@capacitor/app';
import { IonButtons, IonContent, IonHeader, IonTitle, IonToolbar, ModalController, Platform, IonButton, IonLabel, IonItem, IonList, IonFooter } from '@ionic/angular/standalone';
import { Subscription } from 'rxjs';
import { AnalyticsService, IPushData } from '@services';
import { Broadcaster } from '@utils';

@Component({
    selector: 'app-settings',
    styleUrls: ['./settings.page.scss'],
    standalone: true,
    imports: [IonHeader, IonContent, IonToolbar, IonTitle, IonButtons, IonButton, IonLabel, IonItem, IonList, IonFooter],
    template: `
    <ion-header>
      <ion-toolbar networkStatus>
        <ion-title>
          Settings
        </ion-title>
        <ion-buttons slot="primary">
          <ion-button (click)="dismiss()">Close</ion-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <ion-item button lines="inset" detail="true" (click)="onNotificationsClick()">
          <ion-label class="ion-text-wrap">Change Notification settings</ion-label>
        </ion-item>
        <ion-item button lines="inset" detail="true" (click)="onOnboardingClick()">
          <ion-label class="ion-text-wrap">View Onboarding Screens</ion-label>
        </ion-item>
      </ion-list>
    </ion-content>

    <ion-footer class="settings-footer" mode="ios">
      <ion-list>
        <ion-item>
          <img src="assets/images/logo-timebanksw.svg" />
        </ion-item>
        <ion-item class="ion-text-center">
          <ion-label>App version: {{ version }}, build: {{ build }}</ion-label>
        </ion-item>
      </ion-list>
    </ion-footer>
  `
})
export class SettingsPageComponent implements OnInit {
  version: string;
  build: string;
  sub: Subscription;

  constructor(
    private _modal: ModalController,
    private broadcast: Broadcaster,
    private platform: Platform,
    private diagnostic: Diagnostic,
    private analytics: AnalyticsService,
    private router: Router
  ) { }

  ngOnInit() {
    if (!this.platform.is('capacitor')) {
      return;
    }

    App.getInfo().then(result => {
      this.build = result.build;
      this.version = result.version;
    });

    this.sub = this.broadcast
      .subscribe(
        'PushNotificationOpened',
        (payload: IPushData) => this.dismiss()
      );

    this.analytics.setScreenName('page.settings', 'SettingsPage');
  }

  async dismiss() {
    this.sub?.unsubscribe();
    await this._modal.dismiss();
  }

  onNotificationsClick() {
    if (!this.platform.is('capacitor')) {
      return;
    }

    this.diagnostic.switchToSettings();
  }

  onOnboardingClick() {
    this.router.navigateByUrl('/onboarding');
    this.dismiss();
  }
}
