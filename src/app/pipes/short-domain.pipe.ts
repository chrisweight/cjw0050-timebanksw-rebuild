import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'shortDomain',
    standalone: true
})
export class ShortDomainPipe implements PipeTransform {

  transform(url: string): any {
    if (!url || url.length <= 3) {
      return url;
    }

    let result: any, match: RegExpMatchArray;

    if (match = url.match(/^(?:https?:\/\/)?(?:www\.)?([^:\/\n?=]+)/im)) {
      result = match[1];

      if (match = result.match(/^[^.]+\.(.+\..+)$/)) {
        result = match[1];
      }
    }

    return result;
  }
}
