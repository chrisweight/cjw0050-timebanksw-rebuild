import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateFormatterPipe } from './date-formatter.pipe';
import { ShortDomainPipe } from './short-domain.pipe';

@NgModule({
    imports: [CommonModule, DateFormatterPipe, ShortDomainPipe],
    exports: [DateFormatterPipe, ShortDomainPipe],
    providers: [DateFormatterPipe, ShortDomainPipe]
})
export class PipesModule { }
