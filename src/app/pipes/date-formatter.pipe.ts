import { Pipe, PipeTransform } from '@angular/core';
import { parseISO, format } from 'date-fns';

declare type dateFormat = 'shortTime' | 'mediumDate';

@Pipe({
    name: 'dateFormatter',
    standalone: true
})
export class DateFormatterPipe implements PipeTransform {
  transform(date: string, args: dateFormat = 'mediumDate'): any {
    let _f;

    switch (args) {
      case 'shortTime':
        _f = 'h:mm a';
        break;
      case 'mediumDate':
      default:
        _f = 'MMMM do, yyyy';
        break;
    }

    const _d = parseISO(date);
    const _r = format(_d, _f);

    return _r;
  }
}
