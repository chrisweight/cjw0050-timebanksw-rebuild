import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { ApiService, IPost, PostCategory, ShareService, AnalyticsService } from '@services';
import { ResponsiveImageComponent } from '../components/responsive-image/responsive-image.component';
import { IonButtons, IonContent, IonFab, IonFabButton, IonFooter, IonIcon } from '@ionic/angular/standalone';
import { arrowBack, share } from 'ionicons/icons';
import { addIcons } from 'ionicons';

@Component({
    selector: 'app-post',
    styleUrls: ['./post.page.scss'],
    standalone: true,
    imports: [
        RouterLink,
        ResponsiveImageComponent,
        IonContent, IonButtons, IonIcon, IonFooter, IonFab, IonFabButton
    ],
    template: `
      <ion-content>
	      <section class="tsw-post--title--container">
          <ion-buttons>
            <ion-fab-button slot="start" [routerLink]="'/tabs/tab-' + routerLink" routerDirection="back" color="light"
              size="small">
              <ion-icon name="arrow-back">
              </ion-icon>
            </ion-fab-button>
          </ion-buttons>

          <app-responsive-image [src]="image" class="placeholder-image"></app-responsive-image>

          <div class="tsw-post--title--header ion-padding">
            <h2 class="ion-no-margin" [innerHTML]="post.title.rendered"></h2>
          </div>
        </section>

        <section class="tsw-post--content--container">
          <div [innerHTML]="post.content.rendered"></div>
        </section>
      </ion-content>

      <ion-footer>
        <ion-fab vertical="bottom" horizontal="end">
          <ion-fab-button (click)="onShare()">
            <ion-icon name="share"></ion-icon>
          </ion-fab-button>
        </ion-fab>
      </ion-footer>
`
})
export class PostPageComponent implements OnInit {
  post: IPost;
  image: string;
  routerLink: string;
  category: PostCategory;

  constructor(
    private activated: ActivatedRoute,
    private api: ApiService,
    private _share: ShareService,
    private analytics: AnalyticsService
  ) {
    addIcons({ arrowBack, share });
  }

  ngOnInit() {
    this.post = this.activated.snapshot.data.post;
    console.dir(this.post);

    this.category = String(this.post.categories[0]) as PostCategory;

    switch (this.category) {
      case PostCategory.News:
        this.routerLink = 'news';
        break;
      case PostCategory.Stories:
        this.routerLink = 'stories';
        break;
    }

    const i = this.api.postHasImage(this.post);

    if (typeof (i) === 'string') {
      this.image = i;
    }
  }

  ionViewDidEnter() {
    this.analytics.setScreenName('page.post', 'PostPage');

    this.analytics.logEvent({
      name: 'view',
      params: {
        type: 'item.post',
        id: this.post.id,
        title: this.post.title.rendered,
        url: this.post.link
      }
    });
  }

  onShare() {
    const shareType = this.category === PostCategory.News
      ? 'item.news'
      : 'item.story';

    this._share.share(shareType, this.post.id, this.post.title.rendered, this.post.link);
  }
}
