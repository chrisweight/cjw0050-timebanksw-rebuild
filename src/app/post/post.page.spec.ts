import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PostPageComponent } from './post.page';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { IPost } from '@services';
import { ActivatedRoute } from '@angular/router';

describe('PostPage', () => {
  let component: PostPageComponent;
  let fixture: ComponentFixture<PostPageComponent>;

  const post: IPost = {
    id: 0,
    categories: [],
    link: '',
    title: {
      rendered: ''
    },
    excerpt: {
      protected: false,
      rendered: ''
    },
    content: {
      rendered: ''
    },
    featured_media: 0
  }

  const fakeActivatedRoute = {
    snapshot: {
      data: {
        post
      }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [RouterTestingModule, HttpClientTestingModule, PostPageComponent],
    providers: [HTTP, SocialSharing, {provide: ActivatedRoute, useValue: fakeActivatedRoute}]
}).compileComponents();

    fixture = TestBed.createComponent(PostPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
