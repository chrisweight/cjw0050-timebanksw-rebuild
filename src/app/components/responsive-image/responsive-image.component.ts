import {
	Component,
	Input,
	ChangeDetectionStrategy,
	ElementRef,
	Renderer2
} from '@angular/core';
import { isValidString } from '@utils';

export type ResponsiveImageSizing = 'cover' | 'contain';

@Component({
    selector: 'app-responsive-image',
    styleUrls: ['./responsive-image.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    standalone: true,
		template: `
			<div class="container"></div>
		`
})
export class ResponsiveImageComponent {

	@Input()
	set src(value: string) {
		if (!isValidString(value) || value === this._src) {
			return;
		}

		this._src = value;
		this.addIntersectionObserver();
	}

	@Input()
	set sizing(value: ResponsiveImageSizing) {
		this.updateStyle('background-size', `${value}`);
	}

	private _src: string;
	private io: IntersectionObserver;

	constructor(
		public element: ElementRef,
		public renderer: Renderer2
	) { }

	// Private --
	//

	private updateStyle(styleName, styleValue) {
		const _el = this.element.nativeElement.querySelector('.container');
		this.renderer.setStyle(_el, styleName, styleValue);
	}

	private updateImage() {
		this.updateStyle('background-image', `url('${this._src}')`);
	}

	private removeIntersectionObserver() {
		if (!!this.io) {
			this.io.disconnect();
			this.io = null;
		}
	}

	private addIntersectionObserver() {
		// Fall back to setTimeout for Safari and IE
		if (!('IntersectionObserver' in window)) {
			setTimeout(() => this.updateImage(), 0);
			return;
		}

		this.removeIntersectionObserver();

		this.io = new IntersectionObserver((data: any) => {
      if (data.length === 1 && !data[0].isIntersecting) {
        return;
      }

			this.updateImage();
			this.removeIntersectionObserver();
		});

		this.io.observe(this.element.nativeElement);
	}
}
