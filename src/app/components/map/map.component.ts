import {
  Component,
  Input,
  ElementRef,
  ViewChild,
  AfterViewInit,
  NgZone
} from '@angular/core';
import {
  Map,
  tileLayer,
  latLng,
  LatLngExpression,
  marker,
  divIcon,
  MarkerOptions,
  Marker
} from 'leaflet';

import { take } from 'rxjs/operators';

import { environment } from '@environments';
import { GeoService, IEventVenue } from '@services';


const { tilesUrl, attribution, maxZoom, defaultZoom, lat, lng } = environment.osm;

@Component({
    selector: 'app-map',
    styleUrls: ['./map.component.scss'],
    standalone: true,
    template: `
      <div #map></div>
    `
})
export class MapComponent implements AfterViewInit {
  @Input() venue: IEventVenue;
  @ViewChild('map', { static: true }) el: ElementRef;

  map: Map;
  ll: LatLngExpression = latLng(lat, lng);

  constructor(private geo: GeoService, private zone: NgZone) {}

  ngAfterViewInit() {
    this.geo
      .search(this.venue)
      .pipe(take(1))
      .subscribe((latlng: LatLngExpression) => {
        this.ll = latlng || this.ll;

        this.render();
      });
  }

  render() {
    const options = { attribution, maxZoom };

    this.map = new Map(this.el.nativeElement)
      .fitWorld()
      .addLayer(tileLayer(tilesUrl, options))
      .setView(this.ll, defaultZoom);

    const mo: MarkerOptions = {
      icon: divIcon({
        html: `<ion-icon name="pin" size="large" color="primary"></ion-icon>`,
        shadowSize: [0, 0]
      })
    };

    const m: Marker = marker(this.ll, mo);
    m.addTo(this.map);

    // Hack: fix mispositioned rendering of tiles
    this.zone.run(() => {
      setTimeout(() => this.map.invalidateSize(), 400);
    });
  }
}
