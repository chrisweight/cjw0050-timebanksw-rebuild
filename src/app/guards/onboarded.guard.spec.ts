import { TestBed, inject, waitForAsync } from '@angular/core/testing';

import { OnboardedGuard } from './onboarded.guard';

describe('OnboardedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OnboardedGuard]
    });
  });

  it('should ...', inject([OnboardedGuard], (guard: OnboardedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
