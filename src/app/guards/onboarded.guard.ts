import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { OnboardingService } from '@services';

@Injectable({
  providedIn: 'root'
})
export class OnboardedGuard  {
  constructor(private service: OnboardingService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    return this.service.isOnboard().then(result => {
      if (result !== true) {
        this.router.navigate(['/onboarding']);
      }

      return result;
    });
  }
}
