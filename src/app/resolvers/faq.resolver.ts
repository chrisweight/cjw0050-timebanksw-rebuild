import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ApiService, IPost, StorageKey, StorageService } from '@services';


@Injectable({
  providedIn: 'root'
})
export class FaqResolver  {
  constructor(private api: ApiService, private storage: StorageService) { }

  async resolve(route: ActivatedRouteSnapshot) {
    const id = parseInt(route.paramMap.get('id'), 10);
    const existing = await this.storage.loadItemById(StorageKey.Faqs, id);

    if (!!existing) {
      return existing;
    }

    const data = await this.api.getFaqById(id);

    return this.storage.append(StorageKey.Faqs, data);
  }
}
