import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ApiService, IEvent, StorageService, StorageKey } from '@services';

@Injectable({
  providedIn: 'root'
})
export class EventResolver  {
  constructor(private api: ApiService, private storage: StorageService) { }

  async resolve(route: ActivatedRouteSnapshot) {
    const id = parseInt(route.paramMap.get('id'), 10);
    const existing = await this.storage.loadItemById(StorageKey.Events, id);

    if (!!existing) {
      return existing;
    }

    const data = await this.api.getEventById(id);

    return this.storage.append(StorageKey.Events, data);
  }
}
