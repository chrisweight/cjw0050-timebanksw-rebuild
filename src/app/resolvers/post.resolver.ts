import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ApiService, IPost, StorageKey, StorageService } from '@services';


@Injectable({
  providedIn: 'root'
})
export class PostResolver  {
  constructor(private api: ApiService, private storage: StorageService) { }

  async resolve(route: ActivatedRouteSnapshot) {
    const key = this.parseKey(route);
    const id = parseInt(route.paramMap.get('id'), 10);
    const existing = await this.storage.loadItemById(key, id);

    console.log(route, key);

    if (!!existing) {
      return existing;
    }

    const data = await this.api.getPostById(id);

    return this.storage.append(key, data);
  }

  parseKey(route: ActivatedRouteSnapshot): StorageKey {
    switch (route.url[0].path) {
      case 'news':
        return StorageKey.News;

      case 'story':
        return StorageKey.Stories;
    }
  }
}
