import { IEventVenue } from '@services';

export function getLocation(venue: IEventVenue): any {
  const a = venue.address ? `${venue.address}, ` : '';
  const ci = venue.city ? `${venue.city}, ` : '';
  const p = venue.province ? `${venue.province}, ` : '';
  const z = venue.zip ? `${venue.zip}` : '';

  return `${a}${ci}${p}${z}`;
}
