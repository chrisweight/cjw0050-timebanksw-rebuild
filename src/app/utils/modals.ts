import { Dialog, ConfirmResult } from '@capacitor/dialog';

export class Modal {
  public static async permissionRequest(title: string, message: string, callback: Function) {
    return Dialog.confirm({ title, message })
      .then((result: ConfirmResult) => {
        if (result.value !== true) {
          return;
        }

        callback();
      });
  }

  public static async error(title: string, message: string) {
    return Dialog.alert({ title, message });
  }
}
