import { Component, Directive, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IonContent, IonInfiniteScroll } from '@ionic/angular/standalone';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  AnalyticsService,
  ApiService,
  IPost,
  NetworkService,
  StorageKey,
  StorageService
  } from '@services';
import { Broadcaster } from './broadcaster';
import { Request } from './request';


@Directive()
export abstract class ListingsPage implements OnInit, OnDestroy {

  @ViewChild(IonContent) content: IonContent;
  @ViewChild(IonInfiniteScroll, { read: IonInfiniteScroll }) scroll: IonInfiniteScroll;

  key: StorageKey;
  data: any[];
  request: Request;
  destroy: Subject<void> = new Subject<void>();
  connected: boolean;
  firstLoad = true;
  loading = false;

  constructor(
    protected api: ApiService,
    protected storage: StorageService,
    protected network: NetworkService,
    protected analytics: AnalyticsService,
    protected broadcast: Broadcaster,
    protected pageName: string,
  ) { }

  abstract getRequest(): Request;
  abstract save(result: IteratorResult<any>, clear: boolean);

  ngOnInit() {
    this.network.status
      .pipe(takeUntil(this.destroy))
      .subscribe(status => this.connected = status.connected);

    this.broadcast
      .subscribe(
        'ScrollToTop',
        tab => {
          if (tab !== this.pageName) {
            return;
          }

          this.scrollToTop();
        },
        this.destroy
      );
  }

  ngOnDestroy() {
    this.destroy.next();
  }

  ionViewDidEnter() {
    this.analytics.setScreenName(this.pageName, 'ListingsPage');

    if (!this.firstLoad) {
      return;
    }

    this.firstLoad = false;

    if (!this.connected) {
      console.log('ListingsPage.ionViewDidEnter() -> Not connected on first load, loading local store');

      this.storage
        .load(this.key)
        .then(result => this.data = result)
        .catch(console.error);

      return;
    }

    this.doRefresh();
  }

  doRefresh(event?) {
    if (!!this.request) {
      this.request = null;
    }

    this.scroll.disabled = false;
    this.request = this.getRequest();

    this.loadNext(event, true);
  }

  loadNext(event, clear?: boolean) {
    if (this.request.isDone()) {
      return this.complete(true, event);
    }

    this.loading = true;

    this.request
      .next()
      .then(result => {
        this.save(result, clear);
        this.complete(result.done, event);
      })
      .catch(error => {
        if (!this.data) {
          this.data = [];
        }
        this.complete(true, event, error);
      });
  }

  complete(done, event?, error?) {
    this.loading = false;

    if (!event) {
      return;
    }

    event.target.complete();
    event.target.disabled = (done || !!error);
  }

  append(items: IPost[]) {
    return [...(this.data || []), ...items];
  }

  scrollToTop() {
    this.content.scrollToTop(1500);
  }
}
