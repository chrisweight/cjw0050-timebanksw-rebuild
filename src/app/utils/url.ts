export function sanitizeUrl(url: string): string {
  let _u = url;

  if (!/^https?:\/\//i.test(_u)) {
    _u = 'http://' + _u;
  }

  return _u;
}

export async function blobToDataUrl(blob: Blob): Promise<string> {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();

    fileReader.onload = (evt: ProgressEvent) => resolve(evt.target['result']);
    fileReader.onerror = reject;

    fileReader.readAsDataURL(blob);
  });
}
