import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class Broadcaster {

  private _handler: Subject<any> = new Subject<any>();

  constructor() { }

  emit(type: string, payload: any = null) {
    this._handler.next({ type, payload });
  }

  subscribe(type: string, callback: (payload: any) => void, destructor?: Subject<any>): Subscription {
    const _h = this._handler
      .pipe(
        filter(message => message.type === type),
        map(message => message.payload),
      );

    if (!!destructor) {
      _h.pipe(takeUntil(destructor));
    }

    return _h.subscribe(callback);
  }
}
