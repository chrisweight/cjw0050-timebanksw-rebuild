import { HttpParams } from '@angular/common/http';
import { RequestRouter } from './http';

export class Request implements AsyncIterable<any>, AsyncIterator<any> {
	private index = 1;
	private per_page = 10;
	private done = false;

	constructor(
		private router: RequestRouter,
		public url: string,
		perPage?: number,
		private params?: HttpParams
	) {
		if (!!perPage) {
			this.per_page = perPage;
		}
	}

	// Public
	//

	public isDone() {
		return this.done;
	}

	public [Symbol.asyncIterator](): AsyncIterator<any> {
		return this;
	}

	public async next(value?: any): Promise<IteratorResult<any>> {
		console.log('Request.next() -> ', this.url, this.index);

		if (this.done) {
			console.log(this.url, 'done!');

			return {
				done: this.done,
				value: null
			};
		}

		return this.router
			.get(this.url, this.getParams())
			.then(result => {
				if (!!result.count) {
					this.done = this.index >= result.total_pages;
				} else if (Array.isArray(result)) {
					this.done = result.length < this.per_page;
				}

				this.index++;

				return {
					value: result,
					done: this.done
				};
			})
			.catch(error => {
				this.done = true;

				return Promise.reject({
					done: this.done,
					error: error
				});
			});
	}

	// Internal

	private getParams() {
		let p = new HttpParams()
			.set('status', `publish`)
			.set('per_page', this.per_page.toString())
			.set('page', this.index.toString())
			.set('noCache', Date.now().toString());

		if (!this.params) {
			return p;
		}

		this.params.keys().map(key => {
			const v = this.params.get(key);

			p = p.has(key)
				? p.set(key, v)
				: p.append(key, v);
		});

		return p;
	}
}
