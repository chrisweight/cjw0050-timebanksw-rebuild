export function selectBestFit(image, width: number) {
  let _closest;
  let _key;

  Object.entries(image.sizes).forEach(([key, value]) => {
    const _w: number = value['width'];
    const diff = Math.abs((width - _w) as number);

    if (isNaN(_closest)) {
      _closest = diff;
      _key = key;
    }

    if (diff < _closest) {
      _closest = diff;
      _key = key;
    }
  });

  return image.sizes[_key].url;
}
