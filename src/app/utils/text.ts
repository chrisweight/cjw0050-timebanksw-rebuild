// Ref: https://github.com/EddyVerbruggen/Calendar-PhoneGap-Plugin/issues/268
export function stripInvalidCharacters(text: string): string {
  return text.replace(/(?:\r\n|\r|\n)/g, ' ');
}

// https://itsolutionstuff.com/post/angularjs-how-to-remove-html-tags-using-filterexample.html
export function stripTags(text: string): string {
  return text.replace(/<[^>]+>/gm, '');
}

export function truncate(
  text: string,
  length: number = 2048,
  ellipses: boolean = true
): string {
  const _l: number = ellipses ? length : length - 3;
  const _truncated = text.substring(0, _l);

  return ellipses ? _truncated + '...' : _truncated;
}

export function getFileExtension(file: string) {
  return file.substr(file.lastIndexOf('.') + 1);
}

export function isValidString(value: string) {
  return value !== null && typeof value !== 'undefined' && /\S/.test(value);
}
