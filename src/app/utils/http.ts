import { Platform } from '@ionic/angular/standalone';
import { HttpParams, HttpClient, HttpResponse } from '@angular/common/http';
import { HTTP, HTTPResponse } from '@awesome-cordova-plugins/http/ngx';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';

export function paramsToObject(params: HttpParams) {
	return params.keys().reduce((object, key) => {
		object[key] = params.get(key);
		return object;
	}, {});
}

@Injectable({
	providedIn: 'root'
})
export class RequestRouter {
	private isNative: boolean;

	constructor(
		private platform: Platform,
		private httpClient: HttpClient,
		private nativeHttp: HTTP
	) {
		this.isNative = this.platform.is('capacitor');
	}

	// Public
	//

	public download(url: string, params?: HttpParams): Promise<Blob> {
		const _p = params || new HttpParams();

		_p.set('responseType', 'blob');

		const request = this.createRequest(url, 'get', _p);

		return request.then((result: HttpResponse<any> | HTTPResponse) =>
			!this.isNative ? result : (result as HTTPResponse).data
		);
	}

	public async get(url: string, params?: HttpParams): Promise<any> {
    console.log('get()', url, params);
    return this.createRequest(
			url,
			'get',
			params,
			undefined
		)
    .then((result: HttpResponse<any> | HTTPResponse) => this.process(result))
    .catch(error => console.error('get(), error:', error));
	}

	// Internal
	//

	private createRequest(
		url: string,
		method: 'get' | 'post',
		params: HttpParams,
		body?: any
	): Promise<any> {
		const _p = params || new HttpParams();

		if (this.isNative) {
			this.nativeHttp.clearCookies();

			return this.nativeHttp.sendRequest(url, {
				method,
				params: paramsToObject(_p),
        responseType: 'json',
        headers: {
          'Accept': 'application/json',
          'Accept-Encoding': 'gzip, deflate, br'
        }
			});
		}

		switch (method) {
			default:
			case 'get':
				return lastValueFrom(this.httpClient.get(url, { params: _p }));
			case 'post':
				return lastValueFrom(this.httpClient.post(url, body, { params: _p }));
		}
	}

	private process(response: HttpResponse<any> | HTTPResponse) {
    console.log('process()', response);

    if (!this.isNative) {
			return response;
		}

		let data: any;

		try {
      const source = (response as HTTPResponse).data;
      console.dir(source);

      data = typeof source === 'object'
        ? source
        : JSON.parse(source);
		} catch (error) {
			console.error(error);
		}

		return data;
	}
}
