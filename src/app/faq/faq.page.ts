import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnalyticsService, IPost } from '@services';
import { IonButtons, IonContent, IonHeader, IonBackButton, IonToolbar } from '@ionic/angular/standalone';

@Component({
    selector: 'app-faq',
    styleUrls: ['./faq.page.scss'],
    standalone: true,
    imports: [IonHeader, IonContent, IonButtons, IonBackButton, IonToolbar],
    template: `
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/tabs/tab2"></ion-back-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>

      <ion-content>
        <section>
          <h1 [innerHTML]="post?.title.rendered"></h1>
        </section>
        <span [innerHTML]="post?.content.rendered"></span>
      </ion-content>
    `
})
export class FaqPageComponent implements OnInit {
  post: IPost;

  constructor(private activated: ActivatedRoute, private analytics: AnalyticsService) { }

  ngOnInit() {
    this.post = this.activated.snapshot.data.faq;
  }

  ionViewDidEnter() {
    this.analytics.setScreenName('page.faq', 'FaqPage');

    this.analytics.logEvent({
      name: 'view',
      params: {
        type: 'item.faq',
        id: this.post.id,
        title: this.post.title.rendered,
        url: this.post.link
      }
    });
  }
}
