import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FaqPageComponent } from './faq.page';
import { ActivatedRoute } from '@angular/router';

describe('FaqPage', () => {
  let component: FaqPageComponent;
  let fixture: ComponentFixture<FaqPageComponent>;

  const post = {
    id: 0,
    categories: [],
    link: '',
    title: {
      rendered: ''
    }, 
    excerpt: {
      protected: false,
      rendered: ''
    },
    content: {
      rendered: ''
    },
    featured_media: 1
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [RouterTestingModule, HttpClientTestingModule, FaqPageComponent],
    providers: [
        {
            provide: ActivatedRoute,
            useValue: {
                snapshot: {
                    data: { post },
                },
            },
        },
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
}).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqPageComponent);
    component = fixture.componentInstance;
    component.post = post;
    fixture.detectChanges();
  }); 

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
