import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
    selector: '[appEnableDoubleTap]',
    standalone: true
})
export class DoubleTapDirective {

	@Output() doubleTap = new EventEmitter();

	constructor() { }

	@HostListener('tap', ['$event'])
	onTap(event) {
		if (event.tapCount === 2) {
			this.doubleTap.emit(event);
		}
	}
}
