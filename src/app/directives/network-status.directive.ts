import { Directive, OnInit, OnDestroy, HostBinding } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { ConnectionStatus } from '@capacitor/network';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { NetworkService } from '@services';

@Directive({
    selector: '[appNetworkStatus]',
    standalone: true
})
export class NetworkStatusDirective implements OnInit, OnDestroy {
  @HostBinding('style') private style: SafeStyle;

  private _style: SafeStyle;
  private styleString =
    '--background: var(--ion-color-danger); --color: var(--ion-color-danger-contrast)';

  private destroy: Subject<void> = new Subject<void>();

  constructor(
    private sanitizer: DomSanitizer,
    private network: NetworkService
  ) {}

  // Lifecycle
  //

  ngOnInit(): void {
    this._style = this.style;

    this.network.status
      .pipe(takeUntil(this.destroy))
      .subscribe(status => this.update(status));
  }

  ngOnDestroy(): void {
    this.destroy.next(null);
  }

  // Internal
  //

  private update(status: ConnectionStatus) {
    console.log('NetworkStatusDirective.update()', status);

    this.style =
      !status || (status.connected && status.connectionType !== 'none')
        ? this._style
        : this.sanitizer.bypassSecurityTrustStyle(this.styleString);
  }
}
