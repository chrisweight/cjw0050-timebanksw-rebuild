import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EventPageComponent } from './event.page';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { DateFormatterPipe } from '../pipes/date-formatter.pipe';
import { ActivatedRoute } from '@angular/router';
import { IEvent } from '@services';

describe('EventDetailPage', () => {
  let component: EventPageComponent;
  let fixture: ComponentFixture<EventPageComponent>;

  const event: IEvent = {
    id: 0,
    title: '',
    cost: '',
    description: '',
    start_date: '',
    end_date: '',
    excerpt: ''
  }

  const fakeActivatedRoute = {
    snapshot: {
      data: {
        event
      }
    }
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [RouterTestingModule, EventPageComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [Calendar, Diagnostic, SocialSharing, DateFormatterPipe, {provide: ActivatedRoute, useValue: fakeActivatedRoute} ]
}).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
