import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { Browser } from '@capacitor/browser';
import { PluginListenerHandle } from '@capacitor/core';
import { isSameDay, parseISO } from 'date-fns';
import { Modal, sanitizeUrl, selectBestFit } from '@utils';
import { environment } from '@environments';
import { DateFormatterPipe } from './../pipes/date-formatter.pipe';
import { AnalyticsService, CalendarService, IEvent, ShareService } from '@services';
import { ShortDomainPipe } from '../pipes/short-domain.pipe';
import { MapComponent } from '../components/map/map.component';
import { NgIf } from '@angular/common';
import { ResponsiveImageComponent } from '../components/responsive-image/responsive-image.component';
import { IonButtons, IonIcon, IonList, IonItem, IonContent, Platform, IonLabel, IonText } from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { basket, mail, pin, link, arrowBack, calendar, share } from 'ionicons/icons';

const { message, subject } = environment.emailEvent;

@Component({
    selector: 'app-event',
    styleUrls: ['./event.page.scss'],
    standalone: true,
    imports: [RouterLink, ResponsiveImageComponent, NgIf, MapComponent, DateFormatterPipe, ShortDomainPipe, IonButtons, IonIcon, IonList, IonItem, IonContent, IonLabel, IonText],
    providers: [ShortDomainPipe, DateFormatterPipe],
    template: `
      <ion-content>
      <section class="tsw-event--title--container">
        <ion-buttons>
          <ion-fab-button slot="start" routerLink="/tabs/tab-events" routerDirection="back" color="light" size="small">
            <ion-icon name="arrow-back">
            </ion-icon>
          </ion-fab-button>
        </ion-buttons>

        <app-responsive-image [src]="image" class="placeholder-image"></app-responsive-image>

        <div class="tsw-event--title--header ion-padding ion-justify-content-center">
          <h2 class="ion-align-self-center ion-no-margin" [innerHTML]="event.title"></h2>
        </div>
      </section>

      <section>
        <ion-list>
          <ion-item *ngIf="!!event.start_date" button (click)="onAdd()" detail="false">
            <ion-icon name="calendar" slot="start"></ion-icon>
            <ion-label>
              <div *ngIf="isMultiDayEvent; then multi_day; else not_multi_day"></div>
              <ng-template #multi_day>
                <p>{{ event.start_date | dateFormatter }} - {{ event.start_date | dateFormatter: 'shortTime' }}</p>
                <p>
                  To {{ event.end_date | dateFormatter }} - {{ event.end_date | dateFormatter: 'shortTime' }}
                </p>
              </ng-template>
              <ng-template #not_multi_day>
                <p>{{ event.start_date | dateFormatter }}</p>
                <p>
                  {{ event.start_date | dateFormatter: 'shortTime' }}
                  <span *ngIf="event.end_date">- {{ event.end_date | dateFormatter: 'shortTime' }}</span>
                </p>
              </ng-template>
              <ion-text>
                <p>Add to Calendar</p>
              </ion-text>
            </ion-label>
          </ion-item>

          <ion-item *ngIf="!!event.venue && event.venue.address" button (click)="onScrollToMap()" detail="false">
            <ion-icon name="pin" slot="start"></ion-icon>
            <ion-label>
              <p [innerHTML]="event.venue.address"></p>
            </ion-label>
          </ion-item>

          <ion-item *ngIf="!!event.cost">
            <ion-icon name="basket" slot="start"></ion-icon>
            <ion-label>
              <p>{{ event.cost }}</p>
            </ion-label>
          </ion-item>

          <ion-item *ngIf="!!url" button (click)="onOpenUrl()" detail="false">
            <ion-icon name="link" slot="start"></ion-icon>
            <ion-label>
              <ion-text>
                <p>{{url | shortDomain}}</p>
              </ion-text>
            </ion-label>
          </ion-item>

          <ion-item *ngIf="!!organizer" button (click)="onContactOrganizer()" detail="false">
            <ion-icon name="mail" slot="start"></ion-icon>
            <ion-label>
              <ion-text>
                <p>Contact us about this event</p>
              </ion-text>
            </ion-label>
          </ion-item>

          <ion-item *ngIf="!!url" button (click)="onShare()" detail="false">
            <ion-icon name="share" slot="start"></ion-icon>
            <ion-label>
              <ion-text>
                <p>Share</p>
              </ion-text>
            </ion-label>
          </ion-item>

        </ion-list>
      </section>

      <section *ngIf="!!event.description" class="tsw-event--about--container">
        <h3>About</h3>
        <p [innerHTML]="event.description"></p>
      </section>

      <section *ngIf="showMap" class="tsw-event--map--container">
        <h3>Location</h3>
        <app-map [venue]="event.venue"></app-map>
      </section>

      </ion-content>
    `
})
export class EventPageComponent implements OnInit {
  @ViewChild(IonContent, { static: true }) content: IonContent;

  event: IEvent;
  image: string;
  isMultiDayEvent: boolean;
  listeners: PluginListenerHandle[] = [];
  showMap: boolean;
  url: string;
  organizer: string;

  constructor(
    private activated: ActivatedRoute,
    private platform: Platform,
    private _calendar: CalendarService,
    private diagnostic: Diagnostic,
    private _share: ShareService,
    private datePipe: DateFormatterPipe,
    private analytics: AnalyticsService
  ) {
    addIcons({ pin, basket, link, mail, arrowBack, calendar, share });
  }

  // Lifecycle
  //

  ngOnInit() {
    this.event = this.activated.snapshot.data.event;

    this.showMap =
      this.event.show_map === true &&
      this.event.venue &&
      this.event.venue.show_map === true;

    this.url = this.event.url || this.event.website;

    if (
      !!this.event.organizer &&
      this.event.organizer.length > 0 &&
      !!this.event.organizer[0].email
    ) {
      this.organizer = this.event.organizer[0].email;
    }

    console.log(this.organizer);

    this.setImage();
    this.setIsMultiDayEvent();
    this.setBrowser();
  }

  ionViewDidEnter() {
    this.analytics.setScreenName('page.event', 'EventPage');

    this.analytics.logEvent({
      name: 'view',
      params: {
        type: 'item.event',
        id: this.event.id,
        title: this.event.title,
        url: this.event.url
      }
    });
  }

  ionViewWillLeave() {
    this.listeners.map(listener => listener.remove());
    this.listeners = [];
  }

  // Internal
  //

  setImage() {
    if (!this.event.image) {
      return;
    }

    this.image = !this.event.image.sizes
      ? this.event.image.url
      : selectBestFit(this.event.image, this.platform.width());
  }

  setIsMultiDayEvent() {
    this.isMultiDayEvent = false;

    if (this.event.start_date && this.event.end_date) {
      this.isMultiDayEvent = !isSameDay(
        parseISO(this.event.start_date),
        parseISO(this.event.end_date)
      );
    }
  }

  async setBrowser() {
    if (!this.url) {
      return;
    }

    this.listeners.push(
      await Browser.addListener('browserPageLoaded', () => console.log('browserPageLoaded'))
    );

    this.listeners.push(
      await Browser.addListener('browserFinished', () => console.log('browserFinished'))
    );
  }

  // Listeners
  //

  async onOpenUrl() {
    this.analytics.logEvent({
      name: 'openUrl',
      params: {
        type: 'item.event',
        id: this.event.id,
        title: this.event.title,
        url: this.event.url
      }
    });

    await Browser.open({
      url: sanitizeUrl(this.url),
      toolbarColor: '#0297a8'
    });
  }

  onScrollToMap() {
    if (!this.showMap) {
      return;
    }

    this.content.scrollToBottom(1000);
  }

  onAdd() {
    this._calendar
      .hasPermissions()
      .then(result => {
        console.log('calendar has permissions?', result)
        if (!result) {
          return Modal.permissionRequest(
            'Permission required!',
            'You need to grant Timebank SW permission to add this event to your calendar!',
            () => this.diagnostic.switchToSettings()
          );
        }

        return this._calendar.add(this.event).then(
          value => {
            this.analytics.logEvent({
              name: 'addToCalendar',
              params: {
                type: 'item.event',
                id: this.event.id,
                title: this.event.title,
                url: this.event.url
              }
            });
          },
          reason => {
            Modal.error(`There's been a problem!`, reason);
          }
        );
      })
      .catch(error => Modal.error(`There's been a problem!`, error));
  }

  onShare() {
    this._share.share('item.event', this.event.id, this.event.title, this.event.url);
  }

  onContactOrganizer() {
    const m = message
      .replace('${title}', this.event.title)
      .replace('${date}', this.datePipe.transform(this.event.start_date));

    this._share.email(subject, this.organizer, m, 'emailAboutEvent');
  }
}
