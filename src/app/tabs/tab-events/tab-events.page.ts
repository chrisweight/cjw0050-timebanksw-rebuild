import { Component } from '@angular/core';
import { IonCard, IonCardTitle, IonContent, IonHeader, IonImg, IonInfiniteScroll, IonInfiniteScrollContent, IonItemDivider, IonLabel, IonRefresher, IonRefresherContent, IonSkeletonText, IonThumbnail, IonTitle, IonToolbar, Platform } from '@ionic/angular/standalone';
import { format, isSameMonth, parseISO } from 'date-fns';
import {
  AnalyticsService,
  ApiService,
  IEvent,
  NetworkService,
  StorageKey,
  StorageService
  } from '@services';
import { Broadcaster, ListingsPage, selectBestFit } from '@utils';
import { DateFormatterPipe } from '../../pipes/date-formatter.pipe';
import { RouterLink } from '@angular/router';
import { NgIf, NgFor } from '@angular/common';

@Component({
    selector: 'app-tab-events',
    styleUrls: ['tab-events.page.scss'],
    standalone: true,
    imports: [NgIf, NgFor, RouterLink, DateFormatterPipe, IonHeader, IonToolbar, IonTitle, IonContent, IonRefresher, IonRefresherContent, IonItemDivider, IonCardTitle, IonLabel, IonImg, IonSkeletonText, IonThumbnail, IonInfiniteScroll, IonCard, IonInfiniteScrollContent],
    template: `
    <ion-header>
      <ion-toolbar networkStatus enableDoubleTap (doubleTap)="scrollToTop()">
        <ion-title>
          What's on?
        </ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content [scrollEvents]="true">
      <ion-refresher slot="fixed" pullFactor="0.5" (ionRefresh)="doRefresh($event)" disabled="{{!connected}}">
        <ion-refresher-content refreshingSpinner="circles"> </ion-refresher-content>
      </ion-refresher>

      <div class="posts-listing" *ngIf="data && data.length > 0">
        <ng-container *ngFor="let event of data; let i = index; trackBy: trackByFn">
          <ion-item-divider *ngIf="checkMonthUpdate(i)" sticky="true">
            <ion-label>
              {{ getDividerDate(event) }}
            </ion-label>
          </ion-item-divider>
          <ion-card button routerLink="/event/{{ event.id }}">
            <section>
              <div class="inner-container">
                <ion-card-title [innerHTML]="event.title"></ion-card-title>
                <div class="event-date">{{ event.start_date | dateFormatter }}</div>
              </div>
            </section>
            <ion-img [src]="setImage(event.image)" class="placeholder-image"></ion-img>
          </ion-card>
        </ng-container>
      </div>

      <div class="posts-loading" *ngIf="!data">
        <ion-card *ngFor="let number of [1, 2, 3]">
          <ion-thumbnail>
            <ion-skeleton-text animated></ion-skeleton-text>
          </ion-thumbnail>
        </ion-card>
      </div>

      <div class="posts-empty" *ngIf="data && data.length === 0">
        <div class="posts-empty--inner">
          <img src="assets/images/events-empty.svg" />
          <h3><strong>Quiet at the moment...</strong><br />Pull down to refresh for more events!</h3>
        </div>
      </div>

      <ion-infinite-scroll threshold="100px" (ionInfinite)="loadNext($event)" [disabled]="!connected">
        <ion-infinite-scroll-content loadingText="Loading more events..."> </ion-infinite-scroll-content>
      </ion-infinite-scroll>
    </ion-content>
  `
})
export class TabEventsPageComponent extends ListingsPage {

  constructor(
    api: ApiService,
    storage: StorageService,
    network: NetworkService,
    analytics: AnalyticsService,
    broadcaster: Broadcaster,
    private platform: Platform,
  ) {
    super(api, storage, network, analytics, broadcaster, 'tab.events');
    this.key = StorageKey.Events;
  }

  getRequest() {
    return this.api.getEvents();
  }

  trackByFn(index: number, item: IEvent) {
    return item ? item.id : index;
  }

  save(result: IteratorResult<any>, clear: boolean) {
    this.data = (clear === true)
      ? [...result.value['events'] || []]
      : this.append(result.value['events'] || []);

    this.storage.save(this.key, this.data);
  }

  setImage(image) {
    if (!image) {
      return;
    }

    if (!image.sizes) {
      return image.url;
    }

    return selectBestFit(image, this.platform.width());
  }

  checkMonthUpdate(index: number) {
    if (index === 0) {
      return true;
    }

    const previous = parseISO(this.data[index - 1].start_date);
    const current = parseISO(this.data[index].start_date);

    return !isSameMonth(previous, current);
  }

  getDividerDate(event: IEvent) {
    const d = parseISO(event.start_date);
    return format(d, 'MMMM yyyy');
  }
}
