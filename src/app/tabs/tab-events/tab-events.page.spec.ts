import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TabEventsPageComponent } from './tab-events.page';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';

describe('TabEventsPage', () => {
	let component: TabEventsPageComponent;
	let fixture: ComponentFixture<TabEventsPageComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, TabEventsPageComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
		providers: [HTTP]
}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TabEventsPageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges(); 
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
