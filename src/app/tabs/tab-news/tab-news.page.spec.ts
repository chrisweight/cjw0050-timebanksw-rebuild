import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TabNewsPageComponent } from './tab-news.page';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';

describe('TabNewsPage', () => {
	let component: TabNewsPageComponent;
	let fixture: ComponentFixture<TabNewsPageComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, TabNewsPageComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
		providers: [HTTP]
}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TabNewsPageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
