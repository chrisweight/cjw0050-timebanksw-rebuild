import { Component } from '@angular/core';
import { IonButton, IonButtons, IonContent, IonFooter, IonHeader, IonIcon, IonInfiniteScroll, IonInfiniteScrollContent, IonItem, IonLabel, IonList, IonRefresher, IonRefresherContent, IonSkeletonText, IonTitle, IonToolbar, ModalController } from '@ionic/angular/standalone';
import {
  AnalyticsService,
  ApiService,
  NetworkService,
  ShareService,
  StorageKey,
  StorageService
  } from '@services';
import { SettingsPageComponent } from '../../settings/settings.page';
import { Broadcaster, ListingsPage } from '@utils';
import { RouterLink } from '@angular/router';
import { NgIf, NgFor } from '@angular/common';
import { addIcons } from 'ionicons';
import { calendar, informationCircle, mailOpen, newspaper, settings, text } from 'ionicons/icons';


@Component({
    selector: 'app-tab-faqs',
    styleUrls: ['tab-faqs.page.scss'],
    standalone: true,
    imports: [NgIf, NgFor, RouterLink, IonHeader, IonToolbar, IonTitle, IonButtons, IonButton, IonContent, IonRefresher, IonRefresherContent, IonList, IonIcon, IonLabel, IonItem, IonSkeletonText, IonInfiniteScroll, IonInfiniteScrollContent, IonFooter],
    template: `
    <ion-header>
      <ion-toolbar networkStatus enableDoubleTap (doubleTap)="scrollToTop()">
        <ion-title>
          About Timebanking
        </ion-title>
        <ion-buttons slot="primary">
          <ion-button (click)="openSettings()">
            <ion-icon slot="end" name="settings"></ion-icon>
          </ion-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-refresher slot="fixed" pullFactor="0.5" (ionRefresh)="doRefresh($event)" disabled="{{!connected}}">
        <ion-refresher-content refreshingSpinner="circles"> </ion-refresher-content>
      </ion-refresher>

      <div class="posts-listing" *ngIf="data && data.length > 0">
        <ion-list>
          <ion-item *ngFor="let faq of data" routerLink="/faq/{{ faq.id }}" button lines="inset" detail="true"
            routerDirection="forward">
            <ion-label [innerHTML]="faq.title.rendered" class="ion-text-wrap"></ion-label>
          </ion-item>
        </ion-list>
      </div>

      <div class="posts-loading" *ngIf="!data">
        <ion-list>
          <ion-item *ngFor="let number of [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]">
            <ion-skeleton-text animated></ion-skeleton-text>
          </ion-item>
        </ion-list>
      </div>

      <div class="posts-empty" *ngIf="data && data.length === 0">
        <div class="posts-empty--inner">
          <img src="assets/images/faqs-empty.svg" />
          <h3><strong>Couldn't load FAQs</strong><br>Make sure you're online and try again!</h3>
        </div>
      </div>

      <ion-infinite-scroll threshold="100px" (ionInfinite)="loadNext($event)" disabled="{{!connected}}">
        <ion-infinite-scroll-content loadingText="Loading more FAQs...">
        </ion-infinite-scroll-content>
      </ion-infinite-scroll>
    </ion-content>

    <ion-footer class="faqs-footer">
      <ion-button (click)="onContactUsClick()" expand="block">
        <ion-icon slot="start" name="mail-open"></ion-icon>
        Contact us!
      </ion-button>
    </ion-footer>
  `
})
export class TabFaqPageComponent extends ListingsPage {

  constructor(
    api: ApiService,
    storage: StorageService,
    network: NetworkService,
    analytics: AnalyticsService,
    broadcaster: Broadcaster,
    private share: ShareService,
    private modal: ModalController
  ) {
    super(api, storage, network, analytics, broadcaster, 'tab.faqs');
    this.key = StorageKey.Faqs;
    addIcons({ settings, mailOpen });
  }

  getRequest() {
    return this.api.getFaqs();
  }

  save(result: IteratorResult<any>, clear: boolean) {
    this.data = (clear === true)
      ? [...result.value || []]
      : this.append(result.value || []);

    this.storage.save(this.key, this.data);
  }

  async openSettings() {
    const modal = await this.modal.create({
      component: SettingsPageComponent
    });

    await modal.present();
  }

  onContactUsClick() {
    this.share.contactUs();
  }
}
