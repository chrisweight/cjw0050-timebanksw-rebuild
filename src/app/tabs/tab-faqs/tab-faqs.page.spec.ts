import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TabFaqPageComponent } from './tab-faqs.page';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { ModalController } from '@ionic/angular/standalone';

describe('TabFaqPage', () => {
	let component: TabFaqPageComponent;
	let fixture: ComponentFixture<TabFaqPageComponent>;

	beforeEach(waitForAsync(() => {
			TestBed.configureTestingModule({
    	imports: [RouterTestingModule, HttpClientTestingModule, TabFaqPageComponent	],
    	providers: [HTTP, SocialSharing],
    	schemas: [CUSTOM_ELEMENTS_SCHEMA],
		}).compileComponents();
		
		TestBed.overrideComponent(TabFaqPageComponent, {
      set: {
          providers: [
            {
              provide: ModalController, 
              useValue: () => { 
                dismiss: () => Promise.resolve() 
              }
            }
        ],
      },
    });
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TabFaqPageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
