import { Component } from '@angular/core';
import {
  AnalyticsService,
  ApiService,
  IPost,
  NetworkService,
  PostCategory,
  StorageKey,
  StorageService
  } from '@services';
import { Broadcaster, ListingsPage } from '@utils';
import { RouterLink } from '@angular/router';
import { NgIf, NgFor } from '@angular/common';
import { IonCard, IonCardTitle, IonContent, IonHeader, IonImg, IonInfiniteScroll, IonInfiniteScrollContent, IonRefresher, IonRefresherContent, IonSkeletonText, IonThumbnail, IonTitle, IonToolbar } from '@ionic/angular/standalone';


@Component({
    selector: 'app-tab-stories',
    styleUrls: ['tab-stories.page.scss'],
    standalone: true,
    imports: [NgIf, NgFor, RouterLink, IonHeader, IonToolbar, IonTitle, IonContent, IonRefresher, IonRefresherContent, IonCard, IonCardTitle, IonImg, IonThumbnail, IonSkeletonText, IonInfiniteScroll, IonInfiniteScrollContent],
		template: `
		<ion-header>
			<ion-toolbar networkStatus enableDoubleTap (doubleTap)="scrollToTop()">
				<ion-title>
					Timebank Stories
				</ion-title>
			</ion-toolbar>
		</ion-header>

		<ion-content [scrollEvents]="true">
			<ion-refresher slot="fixed" pullFactor="0.5" (ionRefresh)="doRefresh($event)" disabled="{{!connected}}">
				<ion-refresher-content refreshingSpinner="circles"> </ion-refresher-content>
			</ion-refresher>

			<div class="posts-listing" *ngIf="data && data.length > 0">
				<ng-container *ngFor="let item of data; let i = index; trackBy: trackByFn">
					<ion-card button routerLink="/story/{{ item.id }}">
						<section>
							<div class="inner-container">
								<ion-card-title [innerHTML]="item.title.rendered"></ion-card-title>
							</div>
						</section>
						<ion-img [src]="setImage(item)" class="placeholder-image"></ion-img>
					</ion-card>
				</ng-container>
			</div>

			<div class="posts-loading" *ngIf="!data">
				<ion-card *ngFor="let number of [1, 2, 3]">
					<ion-thumbnail>
						<ion-skeleton-text animated></ion-skeleton-text>
					</ion-thumbnail>
				</ion-card>
			</div>

			<div class="posts-empty" *ngIf="data && data.length === 0">
				<div class="posts-empty--inner">
					<img src="assets/images/faqs-empty.svg" />
					<h3><strong>Quiet at the moment...</strong><br />Pull down to refresh for more stories!</h3>
				</div>
			</div>

			<ion-infinite-scroll threshold="100px" (ionInfinite)="loadNext($event)" [disabled]="!connected">
				<ion-infinite-scroll-content loadingText="Loading more stories..."> </ion-infinite-scroll-content>
			</ion-infinite-scroll>
		</ion-content>
	`
})
export class TabStoriesPageComponent extends ListingsPage {

	constructor(
		api: ApiService,
		storage: StorageService,
		network: NetworkService,
    analytics: AnalyticsService,
		broadcaster: Broadcaster
	) {
		super(api, storage, network, analytics, broadcaster, 'tab.stories');
		this.key = StorageKey.Stories;
	}

	getRequest() {
		return this.api.getPosts(PostCategory.Stories);
	}

	trackByFn(index: number, item: IPost) {
		return item ? item.id : index;
	}

	save(result: IteratorResult<any>, clear: boolean) {
		this.data = (clear === true)
			? [...result.value || []]
			: this.append(result.value || []);

		this.storage.save(this.key, this.data);
	}

	setImage(item: IPost) {
		const image = this.api.postHasImage(item);

		if (!image) {
			return;
		}

		return image;
	}
}
