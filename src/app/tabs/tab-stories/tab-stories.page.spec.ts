import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TabStoriesPageComponent } from './tab-stories.page';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';

describe('tab-storiesPage', () => {
	let component: TabStoriesPageComponent;
	let fixture: ComponentFixture<TabStoriesPageComponent>;

	beforeEach(waitForAsync(() => {
		TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, TabStoriesPageComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
		providers: [HTTP]
}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TabStoriesPageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
