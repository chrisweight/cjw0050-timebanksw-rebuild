import { Route } from '@angular/router';
import { TabsPageComponent } from './tabs.page';

export const routes: Route[] = [
  {
    path: 'tabs',
    component: TabsPageComponent,
    children: [
      {
        path: 'tab-news',
        children: [
          {
            path: '',
            loadComponent: () => import('./tab-news').then(m => m.TabNewsPageComponent)
          }
        ]
      },
      {
        path: 'tab-events',
        children: [
          {
            path: '',
            loadComponent: () => import('./tab-events').then(m => m.TabEventsPageComponent)
          }
        ]
      },
      {
        path: 'tab-stories',
        children: [
          {
            path: '',
            loadComponent: () => import('./tab-stories').then(m => m.TabStoriesPageComponent)
          }
        ]
      },
      {
        path: 'tab-faqs',
        children: [
          {
            path: '',
            loadComponent: () => import('./tab-faqs').then(m => m.TabFaqPageComponent)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tab-news',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab-news',
    pathMatch: 'full'
  }
];
