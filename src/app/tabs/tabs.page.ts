import { Component } from '@angular/core';
import { Broadcaster } from '@utils';
import { IonIcon, IonLabel, IonTabBar, IonTabButton, IonTabs } from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { calendar, newspaper, text, informationCircle } from 'ionicons/icons'
@Component({
    selector: 'app-tabs',
    styleUrls: ['tabs.page.scss'],
    standalone: true,
    imports: [IonTabs, IonTabBar, IonTabButton, IonIcon, IonLabel],
    template: `
    <ion-tabs>
      <ion-tab-bar slot="bottom">
        <ion-tab-button tab="tab-news" enableDoubleTap (doubleTap)="onDoubleTap($event)">
          <ion-icon name="newspaper"></ion-icon>
          <ion-label>News</ion-label>
        </ion-tab-button>

        <ion-tab-button tab="tab-events" enableDoubleTap (doubleTap)="onDoubleTap($event)">
          <ion-icon name="calendar"></ion-icon>
          <ion-label>What's on?</ion-label>
        </ion-tab-button>

        <ion-tab-button tab="tab-stories" enableDoubleTap (doubleTap)="onDoubleTap($event)">
          <ion-icon name="text"></ion-icon>
          <ion-label>Stories</ion-label>
        </ion-tab-button>

        <ion-tab-button tab="tab-faqs" enableDoubleTap (doubleTap)="onDoubleTap($event)">
          <ion-icon name="information-circle"></ion-icon>
          <ion-label>About</ion-label>
        </ion-tab-button>
      </ion-tab-bar>
    </ion-tabs>
  `
})
export class TabsPageComponent {
  constructor(protected broadcaster: Broadcaster) {
    addIcons({ calendar, newspaper, text, informationCircle });
  }

  onDoubleTap(event: any) {
    const tab: string = event.target.parentElement.tab;

    if (!tab) {
      return;
    }

    // NOTE: Sub-out the hyphen for a period to match analytics names in tab pages, hacky.
    const _t = tab.replace('-', '.');
    // --

    this.broadcaster.emit('ScrollToTop', _t);
  }
}
