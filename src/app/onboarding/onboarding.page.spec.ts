import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OnboardingPageComponent } from './onboarding.page';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { provideLottieOptions } from 'ngx-lottie';
import player from 'lottie-web';

describe('OnboardingPage', () => {
  let component: OnboardingPageComponent;
  let fixture: ComponentFixture<OnboardingPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
    imports: [OnboardingPageComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [Calendar, Diagnostic,   provideLottieOptions({
      player: () => player,
    })]
})
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
