import { CUSTOM_ELEMENTS_SCHEMA, Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, ViewDidEnter, ViewDidLeave } from '@ionic/angular/standalone';
import { AnalyticsService, CalendarService, OnboardingService, PushService } from '@services';
import { AnimationItem } from 'lottie-web';
import { register } from 'swiper/element/bundle';
import { LottieComponent } from 'ngx-lottie';

register();


@Component({
    selector: 'app-onboarding',
    styleUrls: ['./onboarding.page.scss'],
    standalone: true,
    imports: [LottieComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    template: `
      <ion-content>
        <swiper-container
          #swiper
          (swiperslidechangetransitionstart)="lock(true)"
          (swiperslidechangetransitionend)="onTransitionEnd()"
          slot="fixed"
        >
          <swiper-slide id="onboarding--start">
            <img src="assets/images/logo-timebanksw-knockout.svg" class="ion-padding" />
            <ion-button size="large" expand="full" fill="solid" (click)="next()" disabled="{{disableButtons}}">
              Let's get started!
            </ion-button>
          </swiper-slide>

          <swiper-slide id="onboarding--calendar">
            <ng-lottie #animation [options]="lCalendar" (animationCreated)="onAnimCreated($event)"></ng-lottie>

            <h3 class="ion-padding">
              Use Timebank South West to access your calendar so you can save our
              events!
            </h3>
            <ion-button size="large" expand="full" fill="solid" (click)="onCalendar()" disabled="{{disableButtons}}">
              Enable Calendar
            </ion-button>
          </swiper-slide>

          <swiper-slide id="onboarding--notifications">
            <ng-lottie #animation [options]="lNotifications" (animationCreated)="onAnimCreated($event)"></ng-lottie>
            <h3 class="ion-padding">
              Turn on notifications so Timebank South West can let you know in
              realtime when new events are posted!
            </h3>
            <ion-button size="large" expand="full" fill="solid" (click)="onNotifications()" disabled="{{disableButtons}}">
              Enable Notifications
            </ion-button>
          </swiper-slide>
        </swiper-container>
      </ion-content>
    `
})
export class OnboardingPageComponent implements ViewDidEnter, ViewDidLeave {
  @ViewChild('swiper') swiperContainer: ElementRef;
  @ViewChild('swiper-slide', { static: true }) slides: any;

  animations: AnimationItem[] = [];
  animationEventListeners = [];
  index = 0;
  disableButtons = true;

  lCalendar = {
    path: 'assets/animations/5215-loading-checkmark.json',
    autoplay: false,
    loop: false
  };

  lNotifications = {
    path: 'assets/animations/5215-loading-checkmark.json',
    autoplay: false,
    loop: false
  };

  constructor(
    private router: Router,
    private service: OnboardingService,
    private calendar: CalendarService,
    private push: PushService,
    private platform: Platform,
    private analytics: AnalyticsService
  ) { }

  ionViewDidEnter() {
    this.index = 0;
    this.analytics.setScreenName('page.onboarding', 'OnboardingPage');
    this.onTransitionEnd();
  }

  ionViewDidLeave() {
    this.swiperContainer.nativeElement.swiper.slideTo(0, 0);
  }

  onAnimCreated(animation) {
    this.animationEventListeners.push(
      animation.addEventListener('complete', () => {
        if (this.index >= this.animations.length) {
          this.end();

          return;
        }

        this.next();
      })
    );

    this.animations.push(animation);
  }

  onTransitionEnd() {
    this.lock(true);
    this.disableButtons = false;
  }

  onCalendar() {
    this.disableButtons = true;

    if (!this.platform.is('capacitor')) {
      this.playNextAnimation();

      return;
    }

    this.calendar.requestPermissions().finally(() => this.playNextAnimation());
  }

  onNotifications() {
    this.disableButtons = true;

    if (!this.platform.is('capacitor')) {
      this.playNextAnimation();

      return;
    }

    this.push.requestPermissions().finally(() => this.playNextAnimation());
  }

  playNextAnimation() {
    if (this.index >= this.animations.length) {
      return;
    }

    const anim = this.animations[this.index];

    anim.play();

    this.index++;
  }

  async lock(value: boolean) {
    return this.swiperContainer.nativeElement.swiper.isLocked = value;
  }

  async next() {
    return this.lock(false).then(() => this.swiperContainer.nativeElement.swiper.slideNext());
  }

  end() {
    this.index = 0;
    this.animations.forEach(animation => animation.goToAndStop(0));
    this.service.onboardingComplete().then(() => this.router.navigate(['/']));
  }
}
