import { AnalyticsService } from './analytics.service';
import { Injectable } from '@angular/core';
import { Preferences } from '@capacitor/preferences';

@Injectable({
  providedIn: 'root'
})
export class OnboardingService {
  private key = 'is-onboard';

  constructor(private analytics: AnalyticsService) { }

  public async onboardingComplete(): Promise<any> {
    this.analytics.logEvent({
      name: 'onboardingComplete'
    });

    return Preferences.set({ key: this.key, value: JSON.stringify(true) });
  }

  public async isOnboard(): Promise<boolean> {
    return Preferences.get({ key: this.key }).then(result =>
      JSON.parse(result.value)
    );
  }

  public async reset(): Promise<any> {
    return Preferences.set({ key: this.key, value: JSON.stringify(false) });
  }
}
