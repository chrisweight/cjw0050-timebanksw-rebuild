import { AnalyticsService } from './analytics.service';
import { decode } from 'he';
import { environment } from '@environments';
import { Injectable } from '@angular/core';
import { Modal } from '@utils';
import { Share } from '@capacitor/share';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

const { to, subject } = environment.email;

export type ShareType = 'item.story' | 'item.event' | 'item.news';

@Injectable({
  providedIn: 'root'
})
export class ShareService {

  constructor(private social: SocialSharing, private analytics: AnalyticsService) { }

  share(type: ShareType, id: number, itemTitle: string, url?: string) {
    const _type = type.split('.')[1];
    const { dialogTitle, text, title } = environment.share[_type];

    Share
      .share({
        dialogTitle,
        text,
        title,
        url
      })
      .then(done => {
        console.log('ShareService.share() -> shared!');

        this.analytics.logEvent({
          name: 'share',
          params: {
            type,
            id,
            title: itemTitle,
            url
          }
        });
      })
      .catch(error => Modal.error(`Couldn't Share!`, error));
  }

  contactUs() {
    this.email(subject, to, '', 'contactUs');
  }

  async email(subj: string, recipient: string, message: string, analyticsEventName: string) {
    console.log('ShareService.email()', subj, recipient, message, analyticsEventName);

    return this.social
      .canShareViaEmail()
      .then(() => {
        this.social
          .shareViaEmail(decode(message), subj, [recipient])
          .then(() => {
            console.log('ShareService.email() -> Email sent!');

            this.analytics.logEvent({
              name: analyticsEventName
            });
          })
          .catch(error => Modal.error('Problem sending email!', error));
      })
      .catch(error => Modal.error('Problem sending email!', error));
  }
}
