import { PushService } from './push.service';
import { Injectable, Injector } from '@angular/core';
import { GeoService } from './geo.service';
import { Preferences } from '@capacitor/preferences';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private geo: GeoService;
  private push: PushService;

  constructor(private injector: Injector) {
    this.geo = this.injector.get(GeoService);
    this.push = this.injector.get(PushService);
  }

  public async init() {
    console.log('AppService.init()');

    return Promise.all([
      Preferences.migrate(),
      this.geo.init(),
      this.push
        .hasPermissions()
        .then(result =>
          result === true ? this.push.init() : Promise.resolve()
        )
        // This will likely be an incompatible platform (browser) error
        .catch(error => Promise.resolve())
    ]);
  }
}
