import { TestBed } from '@angular/core/testing';

import { PushService } from './push.service';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';

describe('PushService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [Diagnostic]
  }));

  it('should be created', () => {
    const service: PushService = TestBed.inject(PushService);
    expect(service).toBeTruthy();
  });
});
