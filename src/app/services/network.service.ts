import { Injectable, NgZone } from '@angular/core';
import { PluginListenerHandle } from '@capacitor/core';
import { BehaviorSubject } from 'rxjs';
import { Network, ConnectionStatus} from '@capacitor/network'


@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  public readonly status: BehaviorSubject<ConnectionStatus> = new BehaviorSubject<
    ConnectionStatus
  >({
    connectionType: 'unknown',
    connected: true
  });

  private running = false;
  private networkListener: PluginListenerHandle;

  constructor(private zone: NgZone) {
    this.start();
  }

  // Public
  //

  public stop() {
    if (!this.running) {
      return;
    }

    this.running = false;
    this.networkListener.remove();
  }

  public async start() {
    if (this.running) {
      return;
    }

    this.networkListener = await Network.addListener('networkStatusChange', status =>
      this.zone.run(() => this.update(status))
    );

    Network.getStatus().then(status =>
      this.zone.run(() => this.update(status))
    );
  }

  // Internal
  //

  private update(status: ConnectionStatus) {
    console.log('NetworkService.update()', status);
    this.status.next(status);
  }
}
