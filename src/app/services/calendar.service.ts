import { Injectable } from '@angular/core';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { Platform } from '@ionic/angular/standalone';
import { parseISO } from 'date-fns';
import { getLocation, stripInvalidCharacters, stripTags, truncate } from '@utils';
import { IEvent } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  constructor(
    private calendar: Calendar,
    private platform: Platform,
    private diagnostic: Diagnostic
  ) { }

  // Public
  //

  public async hasPermissions() {
    if (!this.platform.is('capacitor')) {
      return this.onIncompatiblePlatform();
    }

    return this.diagnostic.isCalendarAuthorized();
  }

  public async requestPermissions() {
    if (!this.platform.is('capacitor')) {
      return this.onIncompatiblePlatform();
    }

    return this.calendar
      .hasReadWritePermission()
      .then(hasPermission =>
        hasPermission
          ? Promise.resolve(hasPermission)
          : this.diagnostic.requestCalendarAuthorization()
      );
  }

  public async add(event: IEvent) {
    const options = this.parseEvent(event);
    console.log('CalendarService.add()', options);

    return this.requestPermissions().then(() =>
      this.calendar.createEventInteractivelyWithOptions(
        options.title,
        options.location,
        stripTags(options.notes),
        options.startDate,
        options.endDate,
        options.options
      )
    );
  }

  public async find(event) {
    const options = this.parseEvent(event);
    console.log('CalendarService.find()', options);

    return this.requestPermissions().then(() =>
      this.calendar.findEvent(
        options.title,
        options.location,
        null,
        options.startDate,
        options.endDate
      )
    );
  }

  public async delete(event) {
    const options = this.parseEvent(event);
    console.log('CalendarService.delete()', options);

    return this.requestPermissions().then(() =>
      this.calendar.deleteEvent(
        options.title,
        options.location,
        stripTags(options.notes),
        options.startDate,
        options.endDate
      )
    );
  }

  // Internal
  //

  private getOptions(itemId: number): any {
    if (!this.platform.is('capacitor')) {
      return { id: itemId };
    }

    return Object.assign({}, this.calendar.getCalendarOptions(), {
      id: itemId
    });
  }

  private parseEvent(event: IEvent) {
    return {
      title: stripInvalidCharacters(event.title || ''),
      location: getLocation(event.venue),
      notes: truncate(stripInvalidCharacters(event.description || '')),
      startDate: parseISO(event.start_date),
      endDate: parseISO(event.end_date),
      options: this.getOptions(event.id)
    };
  }

  private async onIncompatiblePlatform() {
    return Promise.reject('CalendarService -> Not a compatible plaform!');
  }
}
