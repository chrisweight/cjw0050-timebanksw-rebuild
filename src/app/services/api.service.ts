import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments';
import { Request, RequestRouter } from '@utils';

export interface IEvent {
  id: number;
  title: string;
  cost: string;
  description: string;
  start_date: string;
  end_date: string;
  excerpt: string;
  image?: IEventImage;
  categories?: IEventCategory[];
  venue?: IEventVenue;
  organizer?: Partial<IEventOrganizer>[];
  website?: string;
  url?: string;
  show_map?: boolean;
}

export interface IEventImage {
  url: string;
  id: string;
  extension: string;
  width: number;
  height: number;
  sizes: [IEventImageSize];
}

export interface IEventImageSize {
  [id: string]: {
    width: number;
    height: number;
    url: string;
  };
}

export interface IEventCategory {
  name: string;
  slug: string;
}

export interface IEventVenue {
  url: string;
  venue: string;
  address: string;
  city: string;
  country: string;
  province: string;
  zip: string;
  show_map: boolean;
}

export interface IEventOrganizer {
  id: number;
  website: string;
  phone: string;
  email: string;
}

export interface IPost {
  id: number;
  categories: PostCategory[];
  link: string;
  title: {
    rendered: string;
  };
  excerpt: {
    protected: boolean;
    rendered: string;
  };
  content: {
    rendered: string;
  };
  featured_media: number;
  _embedded?: {
    ['wp:featuredmedia']?: IWPFeaturedMedia[];
  };
}

export interface IWPFeaturedMedia {
  id: number;
  source_url: string;
  media_type: string;
  mime_type: string;
  title: {
    rendered: string;
  };
}

export enum PostCategory {
  News = '7',
  Stories = '9'
}

const { base, events, faqs, posts } = environment.api;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private router: RequestRouter) { }

  // Public
  //

  // Remote

  public getEvents(): Request {
    return new Request(this.router, `${base}${events}/`);
  }

  public async getEventById(id: number): Promise<IEvent> {
    return this.router.get(`${base}${events}/${id}`, this.getParams());
  }

  public getFaqs(): Request {
    const params = new HttpParams()
      .set('_fields', 'id,title,content');

    return new Request(this.router, `${base}${faqs}/`, 15, params);
  }

  public async getFaqById(id: number): Promise<IPost> {
    return this.router.get(`${base}${faqs}/${id}`, this.getParams());
  }

  public getPosts(category: PostCategory): Request {
    const params = new HttpParams()
      .set('categories', category)
      .set('_fields', 'id,title,content,categories,date,link,_links,_embedded')
      .set('_embed', 'true');

    return new Request(this.router, `${base}${posts}/`, 10, params);
  }

  public async getPostById(id: number): Promise<IPost> {
    return this.router.get(`${base}${posts}/${id}`, this.getParams());
  }

  // Local

  public postHasImage(post: IPost): boolean | string {
    if (!post._embedded) {
      return false;
    }

    const fm = post._embedded['wp:featuredmedia'];

    if (!fm || !fm.length) {
      return false;
    }

    return fm[0].source_url;
  }

  // Internal
  //

  private getParams() {
    return new HttpParams()
      .set('status', `publish`)
      .set('noCache', Date.now().toString());
  }
}
