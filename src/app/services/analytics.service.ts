import { Platform } from '@ionic/angular/standalone';
import { Injectable } from '@angular/core';
import { FirebaseAnalytics } from "@capacitor-community/firebase-analytics";


@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  constructor(private platform: Platform) { }

  // Using the logEvent method here as the setScreenname method fails in iOS
  // It just maps to logEvent anyway, so should be all good
  setScreenName(screenName: string, screenClass: string) {
    if (!this.platform.is('capacitor')) {
      return;
    }

    FirebaseAnalytics.logEvent({
      name: 'screen_view',
      params: {
        screen_name: screenName,
        screen_class: screenClass
      }
    })
  }

  logEvent(payload) {
    if (!this.platform.is('capacitor')) {
      return;
    }

    FirebaseAnalytics.logEvent(payload);
  }
}
