import { Injectable } from '@angular/core';
import { Preferences } from '@capacitor/preferences';

export enum StorageKey {
  News = 'news',
  Events = 'events',
  Faqs = 'faqs',
  Stories = 'stories'
}

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() { }

  public async save<T>(key: StorageKey, data: T): Promise<T> {
    return Preferences.set({ key, value: JSON.stringify(data) }).then(() => data);
  }

  public async load(key: StorageKey): Promise<any[]> {
    return Preferences.get({ key }).then(result => JSON.parse(result.value) || []);
  }

  public async loadItemById(key: StorageKey, id: Number) {
    return this.load(key).then(result => result.find(item => item.id === id));
  }

  public async clear(key: StorageKey): Promise<any> {
    return Preferences.remove({ key });
  }

  public async append<T>(key: StorageKey, data: T): Promise<T> {
    return this.load(key)
      .then(local => [...local, ...[data]])
      .then(toStore => this.save(key, toStore))
      .then(() => data);
  }
}
