import { TestBed } from '@angular/core/testing';

import { ShareService } from './share.service';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

describe('ShareService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [SocialSharing]
  }));

  it('should be created', () => {
    const service: ShareService = TestBed.inject(ShareService);
    expect(service).toBeTruthy();
  });
});
