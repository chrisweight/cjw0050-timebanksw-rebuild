import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Preferences } from '@capacitor/preferences';
import { LatLngExpression } from 'leaflet';
import { from, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '@environments';
import { getLocation } from '@utils';
import { IEventVenue } from './api.service';

const { accessToken, searchUrl, searchUrlExtension } = environment.mapbox;

export interface IGeoResponse {
  attribution: string;
  features: any[];
  query: string[];
  type: string;
}

@Injectable({
  providedIn: 'root'
})
export class GeoService {
  private key = 'geocodes';
  private geocodes: any[] = [];

  constructor(private httpClient: HttpClient) {}

  // Public
  //

  public async init(): Promise<any> {
    return this.load();
  }

  public search(venue: IEventVenue): Observable<any> {
    console.log(`GeoService.search(venue: ${venue}`);

    const location = getLocation(venue);
    const existing = this.find(location);

    if (!!existing) {
      return of(existing);
    }

    const url = `${searchUrl}${encodeURIComponent(
      location
    )}${searchUrlExtension}`;
    const params: HttpParams = new HttpParams().set(
      'access_token',
      accessToken
    );

    const request: Observable<any> = from(this.httpClient.get(url, { params }));

    return request.pipe(
      map(data => this.parse(data)),
      tap(feature => this.save(location, feature)),
      catchError(error => {
        console.error(error);
        return of(undefined);
      })
    );
  }

  // Internal
  //

  private parse(response: IGeoResponse): LatLngExpression {
    if (!response.features.length || response.features.length === 0) {
      throw new Error('no results found for that address');
    }

    // We just take the first result as the array is ordered with highest .relevance rating first
    const [lng, lat] = response.features[0].center;

    return { lat, lng };
  }

  private async save(location: string, feature: any): Promise<any> {
    const existing = this.geocodes.filter(
      f => f.location === feature.location
    )[0];

    if (existing) {
      return existing;
    }

    feature.location = location;

    this.geocodes.push(feature);

    return Preferences.set({ key: this.key, value: JSON.stringify(this.geocodes) });
  }

  private async load(): Promise<any> {
    return Preferences.get({ key: this.key }).then(result => {
      this.geocodes = JSON.parse(result.value) || [];

      return this.geocodes;
    });
  }

  private find(location: string) {
    const existing = this.geocodes.filter(gc => gc.location === location);

    if (existing.length) {
      return existing[0];
    }

    return undefined;
  }
}
