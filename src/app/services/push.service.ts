import { Injectable } from '@angular/core';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { FCM } from '@capacitor-community/fcm';
import { PluginListenerHandle } from '@capacitor/core';
import { Platform } from '@ionic/angular/standalone';
import { environment } from '@environments';
import { Broadcaster } from '@utils';

import {
  PushNotifications,
  Token,
  PushNotificationSchema,
  ActionPerformed,
} from '@capacitor/push-notifications';

import { Device } from '@capacitor/device';

const { fcm } = environment;

export type postType = 'story' | 'event' | 'news';

export interface IPushData {
  postType: postType;
  postId: string;
}

@Injectable({
  providedIn: 'root'
})
export class PushService {
  private registrationListener: PluginListenerHandle;
  private registrationErrorListener: PluginListenerHandle;
  private pushNotificationReceivedListener: PluginListenerHandle;
  private pushNotificationActionPerformedListener: PluginListenerHandle;

  constructor(
    private platform: Platform,
    private diagnostic: Diagnostic,
    private broadcast: Broadcaster
  ) { }

  public async hasPermissions() {
    if (!this.platform.is('capacitor')) {
      return this.onIncompatiblePlatform();
    }

    const permissions = this.platform.is('ios')
      ? await this.diagnostic.getRemoteNotificationsAuthorizationStatus()
      : await this.diagnostic.isRemoteNotificationsEnabled();

    console.log(`PushService.hasPermissions() -> ${permissions}`);

    return permissions === 'authorized' || permissions === true;
  }

  public async requestPermissions() {
    console.log(`PushService.requestPermissions() `);

    if (!this.platform.is('capacitor')) {
      return this.onIncompatiblePlatform();
    }

    const permissions = this.platform.is('ios')
      ? await this.diagnostic.getRemoteNotificationsAuthorizationStatus()
      : await this.diagnostic.isRemoteNotificationsEnabled();

    if (
      permissions === 'denied_always' ||
      permissions === 'denied' ||
      permissions === false
    ) {
      const info = await Device.getInfo();
      if (info.platform === 'android' && info.androidSDKVersion >= 33) {
        return this.init();
      }
      return this.diagnostic.switchToSettings();
    }

    console.log(
      `PushService.requestPermissions() -> permissions: ${permissions}`
    );

    if (permissions === 'authorized' || permissions === true) {
      return true;
    }

    return this.init();
  }

  public async init() {
    console.log('PushService.init()');

    await this.removeListeners();
    await this.addListeners();

    await PushNotifications.register();

    return PushNotifications.requestPermissions().then(result => {
      console.log(`PushService.requestPermissions() -> granted: ${result.receive}`);
      return;
    });
  }

  // Internal

  private async addListeners() {
    this.registrationListener = await PushNotifications.addListener(
      'registration',
      (token: Token) => {
        console.log(
          'PushService.init() -> Push registration success, token: ' +
          token.value
        );

        FCM.subscribeTo(fcm)
          .then(response => console.log(`subscribed to ${fcm.topic}`, response))
          .catch(error => console.log(error));
      }
    );

    this.registrationErrorListener = await PushNotifications.addListener(
      'registrationError',
      (error: any) => {
        console.log('Error on registration: ' + JSON.stringify(error));
      }
    );

    this.pushNotificationReceivedListener = await PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        console.log(
          'PushService.init() -> Push received: ' + JSON.stringify(notification)
        );
      }
    );

    this.pushNotificationActionPerformedListener = await PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: ActionPerformed) => {

        console.log(
          'PushService.init() -> Push action performed: ' + JSON.stringify(notification)
        );

        try {
          const data = notification.notification.data;

          if (!(data.postType && data.postId)) {
            return;
          }

          this.broadcast.emit('PushNotificationOpened', data);

        } catch (error) {
          console.error(error);
        }
      }
    );
  }

  private async removeListeners() {
    try {
      await this.pushNotificationReceivedListener.remove();
      await this.pushNotificationActionPerformedListener.remove();
      await this.registrationListener.remove();
      await this.registrationErrorListener.remove();
    } catch (error) {
      // --
    }
  }

  private async onIncompatiblePlatform() {
    return Promise.reject('PushService -> Not a compatible plaform!');
  }
}
