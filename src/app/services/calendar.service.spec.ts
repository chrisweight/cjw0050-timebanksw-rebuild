import { TestBed } from '@angular/core/testing';

import { CalendarService } from './calendar.service';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';

describe('CalendarService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [Calendar, Diagnostic]
  }));

  it('should be created', () => {
    const service: CalendarService = TestBed.inject(CalendarService);
    expect(service).toBeTruthy();
  });
});
