import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AppService } from './app.service';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';

describe('App.ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [Diagnostic]
  }));

  it('should be created', () => {
    const service: AppService = TestBed.inject(AppService);
    expect(service).toBeTruthy();
  });
});
