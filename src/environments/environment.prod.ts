export const environment = {
  production: true,
  api: {
    base: 'https://www.timebanksw.org/wp-json/',
    events: 'tribe/events/v1/events',
    faqs: 'wp/v2/faq',
    posts: 'wp/v2/posts'
  },
  mapbox: {
    searchUrl: 'https://api.mapbox.com/geocoding/v5/mapbox.places/',
    searchUrlExtension: '.json',
    accessToken:
      'pk.eyJ1IjoiY2hyaXN3ZWlnaHQiLCJhIjoiY2p5YTA1azg4MDh5MDNncDdyb204MmZ3eSJ9.c6UTBA1eL22k4Roknd2DnQ'
  },
  osm: {
    tilesUrl: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
    attribution: `
    Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,
    <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>,
    Imagery © <a href="https://www.mapbox.com/">Mapbox</a>
    `,
    defaultZoom: 18,
    maxZoom: 20,
    lat: 50.37139,
    lng: -4.14222
  },
  email: {
    to: 'info@timebanksw.org',
    subject: `I'd like to know more about TimebankSW!`
  },
  emailEvent: {
    subject: `I'd like to know more about this event!`,
    message: 'Can you tell me more about the ${title} you are hosting on ${date} please?'
  },
  share: {
    event: {
      dialogTitle: 'Share TimebankSW Event',
      text: `Hey! I was using the TimebankSW app and I thought you'd be interested in this event!`,
      title: `Check out this event hosted by Timebank SW!`,
    },
    news: {
      dialogTitle: 'Share TimebankSW News',
      text: `Hey! I was using the TimebankSW app and I thought you'd be interested in this news item!`,
      title: `Check out this Timebank SW news!`,
    },
    story: {
      dialogTitle: 'Share TimebankSW Story',
      title: 'Check out this story from Timebank SW!',
      text: `Hey! I was using the TimebankSW app and I thought you'd be interested in this story!`
    }
  },
  fcm: {
    topic: 'global'
  }
};
