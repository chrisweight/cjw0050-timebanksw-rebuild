import player from 'lottie-web';

import { APP_INITIALIZER, enableProdMode } from '@angular/core';
import { AppComponent } from './app/app.component';
import { routes } from './app/app.routes';
import { AppService } from '@services';
import { bootstrapApplication } from '@angular/platform-browser';
import { Calendar } from '@awesome-cordova-plugins/calendar/ngx';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';
import { environment } from '@environments';
import { HTTP } from '@awesome-cordova-plugins/http/ngx';
import { provideIonicAngular, IonicRouteStrategy } from '@ionic/angular/standalone';
import { provideLottieOptions } from 'ngx-lottie';
import { OnboardedGuard } from './app/guards/onboarded.guard';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { RouteReuseStrategy, provideRouter } from '@angular/router';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';


if (environment.production) {
  enableProdMode();

  if (window) {
    window.console.log = () => { };
  }
}

export function init(service: AppService) {
	return () =>
		service
			.init()
			.then(result => console.log('App Bootstrap complete'));
}

// Note we need a separate function as it's required
// by the AOT compiler.
export function playerFactory() {
  return player;
}

bootstrapApplication(AppComponent, {
  providers: [
    provideIonicAngular({ hardwareBackButton: false }),
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy
    },
    provideRouter(routes),
    provideLottieOptions({
      player: playerFactory,
    }),
    provideHttpClient(withInterceptorsFromDi()),
    Calendar,
    Diagnostic,
    HTTP,
    SocialSharing,
    OnboardedGuard,
    AppService,
    {
      provide: APP_INITIALIZER,
      useFactory: init,
      deps: [AppService],
      multi: true
    },
  ]
})
  .catch(err => console.log(err));
